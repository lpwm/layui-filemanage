"""
@File        : app.py
@Description : 
@Time        : 2021/5/23 22:54
@Author      : DexterLien
@Email       : lpwm@qq.com
@Software    : PyCharm
"""
import os

from flask import Flask, jsonify, request, render_template
from pypinyin import lazy_pinyin
from werkzeug.utils import secure_filename

app = Flask(__name__)

# 上传文件保存的路径
ROOT = os.path.join(app.root_path, 'static', 'upload')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/data', methods=['POST'])
def data():
    action = request.args.get('action')
    if action == 'upload':
        # 处理上传文件
        file = request.files.get('file')
        if file:
            filename = secure_filename(''.join(lazy_pinyin(file.filename)))
            file.save(os.path.join(ROOT, filename))
        return jsonify({
            'code': 0,
            'msg': '上传成功'
        })
    if action == 'get_file_list':
        # 获取文件列表数据
        file_list = []
        for f in os.listdir(ROOT):
            file_type = os.path.splitext(f)[-1][1:]  # 文件后缀, 不包含.
            if os.path.isfile(os.path.join(ROOT, f)):
                if file_type in ['jpg', 'bmp', 'gif']:
                    # 图片格式添加缩略图属性
                    file_list.append({
                        'name': f,
                        'type': file_type,
                        'path': f'/static/upload/{f}',
                        'thumb': f'/static/upload/{f}'  # TODO: 直接用原图当缩略图了, 可以再优化一下
                    })
                else:
                    file_list.append({
                        'name': f,
                        'type': file_type,
                        'path': f'/static/upload/{f}'
                    })
            if os.path.isdir(os.path.join(ROOT, f)):
                file_list.append({
                    'name': f,
                    'type': 'directory',
                    'path': f'/static/upload/{f}'
                })
        return jsonify({
            'code': 0,
            'count': len(file_list),
            'data': file_list,
        })

    if action == 'folder':
        # 创建文件夹
        folder = request.form.get('folder')
        path = request.form.get('path')
        os.mkdir(os.path.join(ROOT, path, folder))
        return jsonify({
            'code': 0,
            'msg': '文件夹创建成功'
        })


if __name__ == '__main__':
    app.run(port=80, debug=True)
